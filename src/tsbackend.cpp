/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tsbackend.h"

TSBackend::TSBackend(QObject *parent)
    : QObject(parent)
{
    const qint64 begin = QDateTime::currentMSecsSinceEpoch();

    m_timeseries = new TimeseriesManager(this);
    m_exports    = new ExportManager(this);

    const qint64 duration = QDateTime::currentMSecsSinceEpoch() - begin;
    qInfo() << "TSBackend: initialisation took" << duration << "ms.";
    qInfo() << "#########################";
    qInfo() << "### TSBackend created ###";
    qInfo() << "#########################";
}
