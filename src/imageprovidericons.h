/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QQuickImageProvider>
#include <QDebug>
#include <QMap>

class ImageProviderIcons : public QQuickImageProvider
{
public:
    explicit ImageProviderIcons() : QQuickImageProvider(QQuickImageProvider::Image) {}

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize)
    {
        if (!m_images.contains(id))
            m_images[id] = QImage(":/icons/"+id+".svg");

        const QImage &image = m_images[id];
        if (image.isNull())
            qWarning().noquote() << "ImageProviderIcons: Could not load icon \""+id+"\".";

        if (size)
            *size = image.size();

        const QSize effSize = QSize(
            requestedSize.width()  > 0 ? requestedSize.width()  : image.size().width(),
            requestedSize.height() > 0 ? requestedSize.height() : image.size().height()
            );

        return image.scaled(effSize,Qt::KeepAspectRatio,Qt::SmoothTransformation);
    }

private:
    QMap<QString,QImage> m_images;
};
