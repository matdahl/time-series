/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "exportmanager.h"

ExportManager::ExportManager(QObject *parent)
    : QObject(parent)
{
    const qint64 begin = QDateTime::currentMSecsSinceEpoch();


    const qint64 duration = QDateTime::currentMSecsSinceEpoch() - begin;
    qInfo() << "ExportManager: initialisation took" << duration << "ms.";
}

bool ExportManager::generateExportFile(const QString &filename){
    if (!QDir().mkpath(m_exportDirPath)){
        qWarning().noquote() << "ExportManager: could not create temporary path '"+m_exportDirPath+"' to prepare export file!";
        return false;
    }

    QFile f(m_exportDirPath + "/" + filename);
    if (f.open(QIODevice::WriteOnly)){
        QTextStream os((&f));

        // write column header
        os << write("rowId")
           << write("date");
        for (DataColumn *col : m_timeseries->columns()->list())
            os << write(col->title());
        os << "\n";

        // build map of column letters for references: key=colIdx, value=letter
        QMap<uint,QString> columnLetters;
        uint curCol = 2; // first two columns are for rowId and date
        for (DataColumn *col : m_timeseries->columns()->list())
            columnLetters[col->idx()] = toLetter(curCol++);

        // write data rows
        int curRow = 2;
        QString curRowStr = QString::number(curRow);
        for (DataRow *row : *m_timeseries->rows()){
            os << write(row->uid())
               << write(row->date());
            for (DataColumn *col : m_timeseries->columns()->list()){
                if (m_resolveReferences){
                    os << write(row->contains(col) ? col->formatValue(row->valueAt(col)) : "");
                } else {
                    switch (col->type()) {
                    case DataColumn::RAWDATA:
                        os << write(row->contains(col) ? col->formatValue(row->valueAt(col)) : "");
                        break;
                    case DataColumn::SUM:
                        os << write("="+columnLetters[col->baseColumn1()]+curRowStr+"+"+columnLetters[col->baseColumn2()]+curRowStr);
                        break;
                    case DataColumn::DIFFERENCE:
                        os << write("="+columnLetters[col->baseColumn1()]+curRowStr+"-"+columnLetters[col->baseColumn2()]+curRowStr);
                        break;
                    case DataColumn::PRODUCT:
                        os << write("="+columnLetters[col->baseColumn1()]+curRowStr+"*"+columnLetters[col->baseColumn2()]+curRowStr);
                        break;
                    case DataColumn::QUOTIENT:
                        os << write("="+columnLetters[col->baseColumn1()]+curRowStr+"/"+columnLetters[col->baseColumn2()]+curRowStr);
                        break;
                    case DataColumn::GRADIENT:
                        if (curRow > 1) {
                            if (col->gradientMode() == DataColumn::GRADIENT_ABSOLUTE)
                                os << write("="+columnLetters[col->baseColumn1()]+curRowStr);
                            else
                                os << write("");
                        } else {
                            const QString lastRowStr = QString::number(curRow-1);
                            QString diffStr = "=("+columnLetters[col->baseColumn1()]+curRowStr+"-"+columnLetters[col->baseColumn1()]+lastRowStr+")";
                            if (col->gradientMode() == DataColumn::GRADIENT_ABSOLUTE)
                                os << write(diffStr);
                            else
                                os << write(diffStr+"/(B"+curRowStr+"-B"+lastRowStr+")");
                        }
                        break;
                    case DataColumn::AGGREGATION:
                        if (col->referenceTime() > 0)
                            // always expand aggregations over limited time range
                            os << write(row->contains(col) ? col->formatValue(row->valueAt(col)) : "");
                        else
                            os << write("=SUM("+columnLetters[col->baseColumn1()]+"$2:"+columnLetters[col->baseColumn1()]+curRowStr+")");
                        break;
                    case DataColumn::AVERAGE:
                        if (col->referenceTime() > 0)
                            // always expand averages over limited time range
                            os << write(row->contains(col) ? col->formatValue(row->valueAt(col)) : "");
                        else
                            os << write("=AVERAGE("+columnLetters[col->baseColumn1()]+"$2:"+columnLetters[col->baseColumn1()]+curRowStr+")");
                        break;
                    }
                }
            }
            os  << "\n";
            curRow++;
            curRowStr = QString::number(curRow);
        }
    }

    m_exportFile = m_exportDirPath + "/" + filename;
    emit exportFileChanged();
    return true;
}

void ExportManager::setTimeseries(Timeseries *ts){
    if (m_timeseries == ts)
        return;

    m_timeseries = ts;
    emit timeseriesChanged();
}

void ExportManager::setDelimiter(const QChar &delimiter){
    if (delimiter == m_delimiter)
        return;

    m_delimiter = delimiter;
    emit delimiterChanged();
}

void ExportManager::setDateFormat(const QString &format){
    if (format == m_dateFormat)
        return;

    m_dateFormat = format;
    emit dateFormatChanged();
}

void ExportManager::setResolveReferences(bool resolve){
    if (resolve == m_resolveReferences)
        return;

    m_resolveReferences = resolve;
    emit resolveReferencesChanged();
}

QString ExportManager::write(const uint msg) const{
    return QString::number(msg)+m_delimiter;
}

QString ExportManager::write(const QString &msg) const{
    if (msg.contains(m_delimiter))
        return "\""+msg+"\""+m_delimiter;
    return msg+m_delimiter;
}

QString ExportManager::write(const QDate &msg) const{
    return write(msg.toString(m_dateFormat));
}

QString ExportManager::write(const qreal msg, const int precision) const{
    return write(QString::number(msg,'f',precision));
}

QString ExportManager::toLetter(const uint idx) const{
    const QChar leading  = 'A' + (idx/26 - 1);
    const QChar trailing = 'A' + (idx%26);
    return idx >= 26 ? QString(leading)+QString(trailing) : QString(trailing);
}
