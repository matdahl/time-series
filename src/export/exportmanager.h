/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QStandardPaths>

#include "timeseries/timeseries.h"

class ExportManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Timeseries *timeseries READ timeseries WRITE setTimeseries NOTIFY timeseriesChanged)
    Q_PROPERTY(QString exportFile READ exportFile NOTIFY exportFileChanged)
    Q_PROPERTY(QChar   delimiter  READ delimiter  WRITE setDelimiter  NOTIFY delimiterChanged)
    Q_PROPERTY(QString dateFormat READ dateFormat WRITE setDateFormat NOTIFY dateFormatChanged)
    Q_PROPERTY(bool resolveReferences READ resolveReferences WRITE setResolveReferences NOTIFY resolveReferencesChanged)

public:
    explicit ExportManager(QObject *parent = nullptr);

    // QML properties - getter
    Timeseries *timeseries() const {return m_timeseries;}
    QString exportFile() const {return m_exportFile;}
    QChar   delimiter() const {return m_delimiter;}
    QString dateFormat() const {return m_dateFormat;}
    bool    resolveReferences() const {return m_resolveReferences;}

    // QML interface
    Q_INVOKABLE bool generateExportFile(const QString &filename);

public slots:
    // QML properties - setter
    void setTimeseries(Timeseries *ts);
    void setDelimiter(const QChar &delimiter);
    void setDateFormat(const QString &format);
    void setResolveReferences(bool resolve);

signals:
    void timeseriesChanged();
    void exportFileChanged();
    void delimiterChanged();
    void dateFormatChanged();
    void resolveReferencesChanged();

private:
    Timeseries *m_timeseries;
    QString     m_exportFile;

    QChar   m_delimiter = '\t';
    QString m_dateFormat = "dd.MM.yyyy";
    bool    m_resolveReferences = true;

    QString write(const uint msg) const;
    QString write(const QString &msg) const;
    QString write(const QDate &msg) const;
    QString write(const qreal msg, const int precision = 1) const;

    QString toLetter(const uint idx) const;

    const QString m_exportDirPath = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/matdahl.time-series/export";
};
