/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "tsbackend.h"
#include "timeseries/datacolumn.h"
#include "timeseries/datacolumnmodel.h"
#include "timeseries/datarow.h"
#include "timeseries/datarowmodel.h"
#include "timeseries/datarowcolumnsmodel.h"
#include "timeseries/timeseries.h"
#include "timeseries/timeseriesmodel.h"
#include "timeseries/timeseriesmanager.h"

#include "export/exportmanager.h"

void TSCorePlugin::registerTypes(const char *uri) {
    //@uri TSCore
    qmlRegisterSingletonType<TSBackend>(uri, 1, 0, "TSBackend", [](QQmlEngine*, QJSEngine*) -> QObject* { return new TSBackend; });

    qmlRegisterType<DataColumn>(uri, 1, 0, "DataColumn");
    qmlRegisterType<DataColumnModel>(uri, 1, 0, "DataColumnModel");
    qmlRegisterType<DataRow>(uri, 1, 0, "DataRow");
    qmlRegisterType<DataRowModel>(uri, 1, 0, "DataRowModel");
    qmlRegisterType<DataRowColumnsModel>(uri, 1, 0, "DataRowColumnsModel");
    qmlRegisterType<Timeseries>(uri, 1, 0, "Timeseries");
    qmlRegisterType<TimeseriesModel>(uri, 1, 0, "TimeseriesModel");
    qmlRegisterType<TimeseriesManager>(uri, 1, 0, "TimeseriesManager");

    qmlRegisterType<ExportManager>(uri, 1, 0, "ExportManager");
}
