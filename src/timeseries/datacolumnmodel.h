/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>

#include "datacolumn.h"

class DataColumnModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    enum Roles{
        COLUMNROLE = Qt::UserRole
    };

    explicit DataColumnModel(QObject *parent = nullptr) : QAbstractListModel(parent) {}
    ~DataColumnModel();

    // QML properties - getter
    int count() const {return m_list.size();}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML interface
    Q_INVOKABLE int     idxAt(int idx) const;
    Q_INVOKABLE QString titleAt(int idx) const;
    Q_INVOKABLE QVariantList getDependentIndices(int idx) const;

    Q_INVOKABLE void clear();
    Q_INVOKABLE void insertLocalColumn(DataColumn *col);
    Q_INVOKABLE void removeColumnAt(const int idx);
    Q_INVOKABLE void swap(const int idx1, const int idx2);

    // C++ interface
    QVector<DataColumn*> &list() {return m_list;}
    DataColumn *getByIdx(const int idx) const;
    DataColumn *get(const uint uid) const;
    bool contains(DataColumn *col) const {return col ? contains(col->uid()) : false;}
    bool contains(const uint uid) const;

    void insertColumn(DataColumn *col);
    void removeColumn(const uint uid);
    void resort();

signals:
    void countChanged();

private:
    QVector<DataColumn*> m_list;


    QVector<uint> getDependentIndices(const DataColumn *col) const;
};
