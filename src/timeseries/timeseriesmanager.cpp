/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseriesmanager.h"

TimeseriesManager::TimeseriesManager(QObject *parent)
    : QObject(parent)
{
    m_model       = new TimeseriesModel(this);
    m_newColumns  = new DataColumnModel(this);
    m_editColumns = new DataColumnModel(this);

    TimeseriesList tslist;
    m_db.read(tslist);
    for (Timeseries *ts : qAsConst(tslist))
        doInsertTimeseries(ts);

    qInfo() << "TimeseriesManager initialised.";
}

void TimeseriesManager::insert(const QString &title){
    Timeseries *ts = new Timeseries;
    ts->setTitle(title);

    if (m_db.insert(ts)){
        doInsertTimeseries(ts);
        for (DataColumn *col : qAsConst(m_newColumns->list()))
            ts->insertColumn(col);

    } else {
        qWarning() << "TimeseriesManager::insert() - could not insert time series!";
        delete ts;
    }
}

void TimeseriesManager::update(Timeseries *ts){
    // remove deleted columns
    for (int i=ts->columns()->count()-1; i>=0; i--){
        DataColumn *col = ts->columns()->list().at(i);
        if (!m_editColumns->contains(col))
            ts->removeColumn(col->uid());
    }

    for (DataColumn *col : qAsConst(editColumns()->list())){
        if (col->uid() == 0){
            // insert new columns
            ts->insertColumn(col);
        } else {
            // update existing columns.
            // Only the following properties will be updated:
            //     idx, title, unit, usePreviousAsDefault, defaultValue,
            //     allowNegative, digits, precision,
            //     baseColumn1, baseColumn2, referenceTime
            ts->updateColumn(col);
        }
    }

    // make timeseries recompute all derived values
    ts->refreshValues();

    // clear editColumns model
    m_editColumns->clear();
}

void TimeseriesManager::remove(const uint uid){
    Timeseries *ts = m_model->find(uid);
    if (!ts)
        return;

    disconnect(ts,SIGNAL(updated(Timeseries*)),this,SLOT(onTimeseriesUpdated(Timeseries*)));
    ts->deleteTimeseries();

    if (m_db.remove(uid)){
        m_model->removeTimeseries(uid);
        delete ts;
    }
}

void TimeseriesManager::prepareTimeseriesForEdit(Timeseries *ts){
    // copy columns to editColumns model
    m_editColumns->clear();
    for (DataColumn *col : qAsConst(ts->columns()->list()))
        m_editColumns->insertColumn(new DataColumn(col,m_editColumns));
}

void TimeseriesManager::setSelected(Timeseries *selected){
    if (m_selected == selected)
        return;

    if (m_selected)
        m_selected->deselect();
    if (selected)
        selected->select();

    m_selected = selected;
    emit selectedChanged();
}

void TimeseriesManager::onTimeseriesUpdated(Timeseries *ts){
    if (!m_db.update(ts))
        qWarning() << "TimeseriesManager: Could not update time series in database!";
}

void TimeseriesManager::doInsertTimeseries(Timeseries *ts){
    if (ts->uid() == 0)
        qWarning() << "TimeseriesManager::doInsertTimeseries() - init time series with uid == 0";

    ts->setParent(this);
    ts->init();

    connect(ts,SIGNAL(updated(Timeseries*)),this,SLOT(onTimeseriesUpdated(Timeseries*)));

    m_model->insertTimeseries(ts);
}
