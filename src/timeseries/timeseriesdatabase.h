/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <MDTK/db/abstractdatabaseclient.h>

#include "timeseries.h"

class TimeseriesDatabase : public MDTK::AbstractDatabaseClient
{
public:
    TimeseriesDatabase() {init();}

    // AbstractDatabaseClient interface
    QString name() const {return "timeseries";}
    TableMap tables() const {
        TableMap tables;
        tables["timeseries"] = {
            {
                {"rank",ColumnType::INTEGER},
                {"title",ColumnType::TEXT}
            }
        };
        return tables;
    }

    bool read(TimeseriesList &list);
    bool insert(Timeseries *ts);
    bool update(const Timeseries *ts);
    bool remove(const uint uid);
};
