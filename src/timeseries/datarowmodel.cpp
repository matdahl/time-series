#include "datarowmodel.h"

int DataRowModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant DataRowModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case ROWROLE:
        return QVariant::fromValue<DataRow*>(m_list.at(index.row()));
    case VALUECHANGESROLE: // this role is only to bind items to changes in the value of one column
        return QVariant(QString());
    }
    return QVariant();
}

QHash<int, QByteArray> DataRowModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[ROWROLE]          = "modelRow";
    names[VALUECHANGESROLE] = "modelValueChanges";
    return names;
}

qreal DataRowModel::getLastValue(const uint colId) const{
    if (m_list.isEmpty())
        return 0.;

    for (auto it = m_list.end()-1; it != m_list.begin(); it--){
        if ((*it)->contains(colId))
            return (*it)->valueAt(colId);
    }
    // remains to check if front() matches colId
    if (m_list.front()->contains(colId))
        return m_list.front()->valueAt(colId);
    return 0.;
}

bool DataRowModel::contains(const uint uid) const{
    for (DataRow *row : m_list)
        if (row->uid() == uid)
            return true;
    return false;
}

qreal DataRowModel::valueAt(const int idx, const uint colId) const{
    if (idx < 0 || idx >= m_list.size())
        return 0.;
    return m_list.at(idx)->valueAt(colId);
}

QDate DataRowModel::dateAt(const int idx) const{
    if (idx < 0 || idx >= m_list.size())
        return QDate();
    return m_list.at(idx)->date();
}

DataRow *DataRowModel::at(const int idx) const{
    if (idx < 0 || idx >= m_list.size())
        return nullptr;
    return m_list.at(idx);

}

QList<qreal> DataRowModel::valuesInTimeRange(DataColumn *col, const QDate &firstDate, const QDate &lastDate){
    QList<qreal> values;
    const bool hasFirstDate = firstDate.isValid();
    const bool hasLastDate  = lastDate.isValid();
    for (DataRow *row : m_list){
        if (hasFirstDate && row->date() < firstDate)
            continue;
        if (hasLastDate && row->date() > lastDate)
            continue;
        if (row->contains(col)){
            values.push_back(row->valueAt(col));
        }
    }
    return values;
}

int DataRowModel::insert(DataRow *row){
    row->setParent(this);

    int insertIdx = qMax(m_list.size(),0);
    while (insertIdx > 0 && m_list.at(insertIdx-1)->date() > row->date())
        insertIdx--;

    // if there are multiple rows with the same date, sort by id
    while (insertIdx > 0 && m_list.at(insertIdx-1)->date() == row->date() && m_list.at(insertIdx-1)->uid() > row->uid())
        insertIdx--;

    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    m_list.insert(insertIdx,row);
    endInsertRows();
    emit countChanged();
    return insertIdx;
}

int DataRowModel::remove(const uint uid){
    int idx = 0;
    for (DataRow *row : m_list){
        if (row->uid() == uid){
            beginRemoveRows(QModelIndex(),idx,idx);
            m_list.removeAt(idx);
            endRemoveRows();
            emit countChanged();
            if (row->parent() == this)
                delete row;
            return idx;
        }
        idx++;
    }
    return -1;
}

int DataRowModel::updateValue(DataColumn *col, DataRow *row, const qreal value){
    int idx=0;
    for (DataRow *r : m_list){
        if (r->uid() == row->uid()){
            r->columns()->setValue(col->uid(),value);
            valuesChanged(idx);
            return idx;
        }
        idx++;
    }
    return -1;
}

void DataRowModel::valuesChanged(const int idx, const int lastIdx){
    const int lastIdxEff = qMin(qMax(idx,lastIdx),m_list.size()-1);
    emit dataChanged(index(idx),index(lastIdxEff),{VALUECHANGESROLE});
}
