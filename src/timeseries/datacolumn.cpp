/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datacolumn.h"

DataColumn::DataColumn(QObject *parent) : QObject(parent) {}

DataColumn::DataColumn(DataColumn *col, QObject *parent)
    : DataColumn(parent ? parent : col->parent())
{
    m_uid             = col->uid();
    m_idx             = col->idx();
    m_type            = col->type();
    m_title           = col->title();
    m_unit            = col->unit();
    m_defaultPrevious = col->defaultPrevious();
    m_defaultValue    = col->defaultValue();
    m_allowNegative   = col->allowNegative();
    m_digits          = col->digits();
    m_precision       = col->precision();
    m_baseColumn1     = col->baseColumn1();
    m_baseColumn2     = col->baseColumn2();
    m_scale1          = col->scale1();
    m_scale2          = col->scale2();
    m_referenceTime   = col->referenceTime();
    m_width           = col->width();
}

QString DataColumn::formatValue(const qreal value) const{
    return QString::number(value,'f',m_precision);
}

DataColumn::GradientMode DataColumn::gradientMode() const{
    if (m_type != GRADIENT)
        return GRADIENT_INVALID;
    if (m_referenceTime == 0)
        return GRADIENT_ABSOLUTE;
    if (m_referenceTime == 1)
        return GRADIENT_PERDAY;
    return GRADIENT_INVALID;
}

void DataColumn::setUid(const uint uid){
    if (uid == m_uid)
        return;

    m_uid = uid;
    emit uidChanged();
}
void DataColumn::setIdx(const uint idx){
    if (idx == m_idx)
        return;

    m_idx = idx;
    emit idxChanged();
}
void DataColumn::setType(const Type type){
    if (type == m_type)
        return;

    m_type = type;
    emit typeChanged();
}
void DataColumn::setTitle(const QString &title){
    if (title == m_title)
        return;

    m_title = title;
    emit titleChanged();
}
void DataColumn::setUnit(const QString &unit){
    if (unit == m_unit)
        return;

    m_unit = unit;
    emit unitChanged();
}
void DataColumn::setDefaultPrevious(const bool defaultPrevious){
    if (defaultPrevious == m_defaultPrevious)
        return;

    m_defaultPrevious = defaultPrevious;
    emit defaultPreviousChanged();
}
void DataColumn::setDefaultValue(const qreal defaultValue){
    if (defaultValue == m_defaultValue)
        return;

    m_defaultValue = defaultValue;
    emit defaultValueChanged();
}
void DataColumn::setAllowNegative(const bool allowNegative){
    if (allowNegative == m_allowNegative)
        return;

    m_allowNegative = allowNegative;
    emit allowNegativeChanged();
}
void DataColumn::setDigits(const uint digits){
    const uint digitsEff = qMax(digits,1u);
    if (digitsEff == m_digits)
        return;

    m_digits = digitsEff;
    emit digitsChanged();
}
void DataColumn::setPrecision(const uint precision){
    if (precision == m_precision)
        return;

    m_precision = precision;
    emit precisionChanged();
}
void DataColumn::setBaseColumn1(const int baseColumn1){
    if (baseColumn1 == m_baseColumn1)
        return;

    m_baseColumn1 = baseColumn1;
    emit baseColumn1Changed();
}
void DataColumn::setBaseColumn2(const int baseColumn2){
    if (baseColumn2 == m_baseColumn2)
        return;

    m_baseColumn2 = baseColumn2;
    emit baseColumn2Changed();
}
void DataColumn::setReferenceTime(const int referenceTime){
    if (referenceTime == m_referenceTime)
        return;

    m_referenceTime = referenceTime;
    emit referenceTimeChanged();

}
void DataColumn::setScale1(const qreal value){
    if (value == m_scale1)
        return;

    m_scale1 = value;
    emit scale1Changed();
}
void DataColumn::setScale2(const qreal value){
    if (value == m_scale2)
        return;

    m_scale2 = value;
    emit scale2Changed();
}

void DataColumn::setWidth(const int width){
    if (width == m_width)
        return;

    m_width = width;
    emit widthChanged();
}
