/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QDebug>
#include <QList>
#include <QDir>

#include "datacolumnmodel.h"
#include "datarow.h"
#include "datarowmodel.h"
#include "timeseriescontentdatabase.h"

class Timeseries : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint    uid   READ uid   CONSTANT)
    Q_PROPERTY(int     rank  READ rank                 NOTIFY rankChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)

    Q_PROPERTY(DataColumnModel *columns READ columns CONSTANT)
    Q_PROPERTY(DataRowModel    *rows    READ rows    CONSTANT)

    // UI properties
    Q_PROPERTY(int dataColumnWidth READ dataColumnWidth WRITE setDataColumnWidth NOTIFY dataColumnWidthChanged)

public:
    explicit Timeseries(QObject *parent = nullptr);
    ~Timeseries();

    // to be called when a time series should be deleted permanently
    void deleteTimeseries();

    // QML properties - getter
    uint    uid()   const {return m_uid;}
    int     rank()  const {return m_rank;}
    QString title() const {return m_title;}
    DataColumnModel *columns() const {return m_columns;}
    DataRowModel    *rows()    const {return m_rows;}
    int dataColumnWidth() const {return m_dataColumnWidth;}

    // QML interface
    Q_INVOKABLE QDate getMonthAt(const int row) const;
    Q_INVOKABLE int getBeforeCount(const QDate &month) const;

    Q_INVOKABLE void select();
    Q_INVOKABLE void deselect();

    Q_INVOKABLE void insertRow(DataRow *row);
    Q_INVOKABLE void removeRow(DataRow *row);
    Q_INVOKABLE void updateValue(DataColumn *col, DataRow *row, const qreal value);

    // C++ setter
    void setUid(const uint uid);
    void setRank(const int rank);

    // C++ interface
    void init();

    void insertColumn(DataColumn *col);
    void updateColumn(DataColumn *newCol);
    void removeColumn(const uint uid);

    void refreshValues();

public slots:
    // QML properties - setter
    void setTitle(const QString &title);
    void setDataColumnWidth(int dataColumnWidth);

signals:
    // QML properties - notifies
    void rankChanged();
    void titleChanged();
    void dataColumnWidthChanged();

    void updated(Timeseries *ts);

private:
    uint    m_uid = 0;
    int     m_rank = 0;
    QString m_title;
    bool    m_initialised = false;
    bool    m_isSelected = false;
    bool    m_containsComplexColumns = false;
    int     m_dataColumnWidth = 0;

    DataColumnModel *m_columns = nullptr;
    DataRowModel    *m_rows    = nullptr;

    TimeSeriesContentDatabase *m_db = nullptr;

    // map to track which month starts at which index
    QMap<QDate,int> m_monthlyBeforeCounts;


    QString prompt(const QString &context = QString()) const{
        return "Time series '" + m_title + "' (uid: " +QString::number(m_uid)+")" + (context.size()>0 ? " - "+context : "") + ": ";
    }

    void doInsertRow(DataRow *row);
    void doRecomputeRow(const int idx);

    bool doComputeSumColumn(DataRow *row, DataColumn *col);
    bool doComputeDifferenceColumn(DataRow *row, DataColumn *col);
    bool doComputeProductColumn(DataRow *row, DataColumn *col);
    bool doComputeQuotientColumn(DataRow *row, DataColumn *col);
    bool doComputeGradientColumn(DataRow *row, DataColumn *col, const int insertIdx);
    bool doComputeAggregationColumn(DataRow *row, DataColumn *col);
    bool doComputeAverageColumn(DataRow *row, DataColumn *col);
};

typedef QList<Timeseries*> TimeseriesList;
