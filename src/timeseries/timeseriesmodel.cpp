/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseriesmodel.h"

int TimeseriesModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant TimeseriesModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case TIMESERIESROLE:
        return QVariant::fromValue<Timeseries*>(m_list.at(index.row()));
    }
    return QVariant();
}

QHash<int, QByteArray> TimeseriesModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[TIMESERIESROLE] = "timeseries";
    return names;
}

int TimeseriesModel::indexOf(const uint uid) const{
    for (int i=0; i<m_list.size(); i++)
        if (m_list.at(i)->uid() == uid)
            return i;
    return -1;
}

Timeseries *TimeseriesModel::at(const int idx) const{
    if (idx < 0 || idx >= m_list.size())
        return nullptr;
    return m_list.at(idx);
}

QVariantList TimeseriesModel::titles() const{
    QVariantList list;
    list.reserve(count());
    for (Timeseries *ts : m_list)
        list.append(QVariant(ts->title()));
    return list;
}

Timeseries *TimeseriesModel::find(const uint uid) const{
    for (Timeseries *ts : m_list)
        if (ts->uid() == uid)
            return ts;
    return nullptr;
}

void TimeseriesModel::insertTimeseries(Timeseries *ts){
    int insertIdx = 0;
    while (insertIdx < m_list.size() && m_list.at(insertIdx)->rank() < ts->rank())
        insertIdx++;

    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    m_list.insert(insertIdx,ts);
    endInsertRows();
    emit countChanged();
}

void TimeseriesModel::removeTimeseries(const uint uid){
    for (int i=0; i<m_list.size(); i++){
        if (m_list.at(i)->uid() == uid){
            beginRemoveRows(QModelIndex(),i,i);
            m_list.removeAt(i);
            endRemoveRows();
            emit countChanged();
            return;
        }
    }
}

void TimeseriesModel::swap(const int idx1, const int idx2){
    if (idx1 < 0 || idx1 >= m_list.size() || idx2 < 0 || idx2 >= m_list.size() || idx1 == idx2)
        return;

    const int from = qMax(idx1,idx2);
    const int to   = qMin(idx1,idx2);

    if (beginMoveRows(QModelIndex(),from,from,QModelIndex(),to)){
        Timeseries *ts1 = m_list.at(idx1);
        Timeseries *ts2 = m_list.at(idx2);

        const int rank = ts1->rank();
        ts1->setRank(ts2->rank());
        ts2->setRank(rank);

        m_list.move(from,to);
        endMoveRows();
    }
}
