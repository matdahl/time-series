/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseriescontentdatabase.h"

TimeSeriesContentDatabase::TimeSeriesContentDatabase(const uint tsUid)
    : m_db_name("ts"+QString::number(tsUid))
{
    migrate_v1_1();
    init();
}

TimeSeriesContentDatabase::~TimeSeriesContentDatabase(){
    QSqlDatabase::removeDatabase(m_db_name);
}

void TimeSeriesContentDatabase::handleMigrations(TableMigrationEvent &event){
    if (event.tableName == "columns"){
        if (event.fromScheme == 0 && event.toScheme == 1){
            printInfo("migrate columns table from scheme 0 -> 1");

            // select old data
            QSqlQuery q = selectAll("columns",*event.db);
            if (!q.isActive())
                return;

            QList<QMap<QString,QString>> oldRows;
            while (q.next()){
                QMap<QString,QString> row;
                row["uid"]                  = q.value("uid").toString();
                row["idx"]                  = q.value("idx").toString();
                row["type"]                 = q.value("type").toString();
                row["title"]                = q.value("title").toString();
                row["unit"]                 = q.value("unit").toString();
                row["usePreviousAsDefault"] = q.value("usePreviousAsDefault").toString();
                row["defaultValue"]         = q.value("defaultValue").toString();
                row["allowNegative"]        = q.value("allowNegative").toString();
                row["digits"]               = q.value("digits").toString();
                row["precision"]            = q.value("precision").toString();
                row["baseCol1"]             = q.value("baseCol1").toString();
                row["baseCol2"]             = q.value("baseCol2").toString();
                row["referenceTime"]        = q.value("referenceTime").toString();
                oldRows.append(row);
            }

            q.exec("DROP TABLE columns");
            createTable("columns",tables()["columns"][1],*event.db);
            for (const auto &row : oldRows){
                q.exec("INSERT INTO columns (uid,idx,type,title,unit,usePreviousAsDefault,defaultValue,"
                                            "allowNegative,digits,precision,baseCol1,baseCol2,referenceTime) VALUES "
                       "(" + row["uid"] +
                       "," + row["idx"] +
                       "," + row["type"] +
                       "," + writeText(row["title"]) +
                       "," + writeText(row["unit"]) +
                       "," + writeBoolean(row["usePreviousAsDefault"] == "1") +
                       "," + row["defaultValue"] +
                       "," + writeBoolean(row["allowNegative"] == "1") +
                       "," + row["digits"] +
                       "," + row["precision"] +
                       "," + row["baseCol1"] +
                       "," + row["baseCol2"] +
                       "," + row["referenceTime"] +
                       ")");
                evaluateQuery(q,"migrate_v1_1 - insert into new table \"columns\"");
            }

            printInfo("migration table \"columns\" 0 -> 1 done.");
            event.handled = true;
        }
    }
}

DataColumnList TimeSeriesContentDatabase::readColumns(QObject *parent){
    DataColumnList list;

    QSqlDatabase db = openDatabase();
    QSqlQuery q = selectAll("columns",db);
    if (!q.isActive())
        return list;

    while (q.next()){
        DataColumn *col = new DataColumn(parent);
        try{
            col->setUid(parseUInt(q,"uid"));
            col->setIdx(parseUInt(q,"idx"));
            col->setType((DataColumn::Type) parseInteger(q,"type"));
            col->setTitle(parseText(q,"title"));
            col->setUnit(parseText(q,"unit"));
            col->setDefaultPrevious(parseBoolean(q,"usePreviousAsDefault"));
            col->setDefaultValue(parseReal(q,"defaultValue"));
            col->setAllowNegative(parseBoolean(q,"allowNegative"));
            col->setDigits(parseUInt(q,"digits"));
            col->setPrecision(parseUInt(q,"precision"));
            col->setBaseColumn1(parseUInt(q,"baseCol1"));
            col->setBaseColumn2(parseUInt(q,"baseCol2"));
            col->setReferenceTime(parseInteger(q,"referenceTime"));
            col->setScale1(parseReal(q,"scale1"));
            col->setScale2(parseReal(q,"scale2"));

            list.append(col);
        } catch (std::exception e) {
            printError("readColumns() - could not parse a column: " + QString(e.what()) + " - Ignore and continue with next.");
            delete col;
        }
    }
    return list;
}

bool TimeSeriesContentDatabase::insertColumn(DataColumn *col){
    const QMap<QString,QString> values = {
        {"idx",writeUInt(col->idx())},
        {"type",writeInteger(col->type())},
        {"title",writeText(col->title())},
        {"unit",writeText(col->unit())},
        {"usePreviousAsDefault",writeBoolean(col->defaultPrevious())},
        {"defaultValue",writeReal(col->defaultValue())},
        {"allowNegative",writeBoolean(col->allowNegative())},
        {"digits",writeUInt(col->digits())},
        {"precision",writeUInt(col->precision())},
        {"baseCol1",writeUInt(col->baseColumn1())},
        {"baseCol2",writeUInt(col->baseColumn2())},
        {"referenceTime",writeUInt(col->referenceTime())},
        {"scale1",writeReal(col->scale1())},
        {"scale2",writeReal(col->scale2())}
    };
    const int insertIdx = simpleInsert("columns",values);

    if (insertIdx >= 0){
        col->setUid(insertIdx);
        return true;
    }
    return false;
}

bool TimeSeriesContentDatabase::updateColumn(DataColumn *col){
    const QMap<QString,QString> values = {
        {"idx",writeUInt(col->idx())},
        {"title",writeText(col->title())},
        {"unit",writeText(col->unit())},
        {"usePreviousAsDefault",writeBoolean(col->defaultPrevious())},
        {"defaultValue",writeReal(col->defaultValue())},
        {"allowNegative",writeBoolean(col->allowNegative())},
        {"digits",writeUInt(col->digits())},
        {"precision",writeUInt(col->precision())},
        {"baseCol1",writeUInt(col->baseColumn1())},
        {"baseCol2",writeUInt(col->baseColumn2())},
        {"referenceTime",writeUInt(col->referenceTime())},
        {"scale1",writeReal(col->scale1())},
        {"scale2",writeReal(col->scale2())}
    };
    return simpleUpdate("columns",values,writeUInt(col->uid()));
}

bool TimeSeriesContentDatabase::removeColumn(const uint uid){
    return simpleDelete("columns",writeUInt(uid)) &&
           simpleDelete("datapoints",writeUInt(uid),"colId");
}

TimeSeriesContentDatabase::RowDataMap TimeSeriesContentDatabase::readDataPoints(){
    RowDataMap map;

    QSqlDatabase db = openDatabase();
    QSqlQuery q = selectAll("rows",db);
    if (!q.isActive())
        return map;

    while (q.next()){
        try{
            const uint  rowId = parseUInt(q,"uid");
            const QDate date  = parseDate(q,"date");

            map[rowId] = {date,{}};
        } catch (std::exception e) {
            printError("readDataPoints() - could not parse a row: " + QString(e.what()) + " - Ignore and continue with next.");
        }
    }
    q.finish();

    q = selectAll("datapoints",db);
    if (!q.isActive())
        return map;

    while (q.next()){
        try{
            const uint  rowId = parseUInt(q,"rowId");
            const uint  colId = parseUInt(q,"colId");
            const qreal value = parseReal(q,"value");

            if (map.contains(rowId))
                map[rowId].second[colId] = value;
            else
                printWarning("readDataPoints() - Found a data point to non existing rowId =" + writeUInt(rowId));
        } catch (std::exception e) {
            printError("readDataPoints () - could not parse a datapoint: " + QString(e.what()) + " - Ignore and continue with next.");
        }
    }

    return map;
}

bool TimeSeriesContentDatabase::insertDataRow(DataRow *row){
    const int insertIdx = simpleInsert("rows",{{"date",writeDate(row->date())}});
    if (insertIdx < 0)
        return false;

    row->setUid(insertIdx);

    // create data points
    QMap<QString,QString> values = {{"rowId",writeUInt(row->uid())}};
    for (const DataRowColumnsModel::Node &node : row->columns()->list()){
        values["colId"] = writeUInt(node.column->uid());
        values["value"] = writeReal(node.value);
        if (simpleInsert("datapoints",values) < 0)
            return false;
    }
    return true;
}

bool TimeSeriesContentDatabase::removeDataRow(DataRow *row){
    return simpleDelete("rows",writeUInt(row->uid())) &&
           simpleDelete("datapoints",writeUInt(row->uid()),"rowId");
}

bool TimeSeriesContentDatabase::updateValue(DataColumn *col, DataRow *row, const qreal value){
    const QMap<QString,QString> deleteConditions = {
        {"rowId",writeUInt(row->uid())},
        {"colId",writeUInt(col->uid())}
    };
    if (!multiDelete("datapoints",deleteConditions))
        return false;

    const QMap<QString,QString> values = {
        {"rowId",writeUInt(row->uid())},
        {"colId",writeUInt(col->uid())},
        {"value",writeReal(value)}
    };
    return simpleInsert("datapoints",values) >= 0;
}

void TimeSeriesContentDatabase::migrate_v1_1(){
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",name());
        db.setDatabaseName(filepath());
        if (!db.open()){
            printError("migrate_v1_1() - could not open database: " + db.lastError().text());
            return;
        }

        const bool needToMigrateRows       = hasTable("rows",db)       && !tableHasColumn("rows","uid",db);
        const bool needToMigrateDatapoints = hasTable("datapoints",db) && !tableHasColumn("datapoints","uid",db);
        if (needToMigrateRows || needToMigrateDatapoints){
            // make backup
            QFile::remove(filepath()+".migrationbackup");
            if (QFile::copy(filepath(),filepath()+".migrationbackup")){
                printInfo("created database backup: '"+filepath()+".migrationbackup'");
            } else {
                printError("could not create database backup at '"+filepath()+".migrationbackup'.");
                return;
            }

            // check rows table
            if (needToMigrateRows){
                printInfo("migrate table \"rows\": rename column \"rowId\" -> \"uid\".");

                // select old data
                QSqlQuery q = selectAll("rows",db);
                if (!q.isActive())
                    return;

                QList<QMap<QString,QString>> oldRows;
                while (q.next()){
                    QMap<QString,QString> row;
                    row["uid"]  = q.value("rowId").toString();
                    row["date"] = q.value("date").toString();
                    oldRows.append(row);
                }

                q.exec("DROP TABLE rows");
                createTable("rows",tables()["rows"][0],db);
                for (const auto &row : oldRows){
                    q.exec("INSERT INTO rows (uid,date) VALUES "
                           "(" + row["uid"] +
                           ",'" + row["date"] + "'"
                                                ")");
                    evaluateQuery(q,"migrate_v1_1 - insert into new table \"rows\"");
                }
                printInfo("table \"rows\" migrated.");
            }

            // check datapoints table
            if (needToMigrateDatapoints){
                printInfo("migrate table \"datapoints\": insert column \"uid\".");

                // select old data
                QSqlQuery q = selectAll("datapoints",db);
                if (!q.isActive())
                    return;

                QList<QMap<QString,QString>> oldRows;
                while (q.next()){
                    QMap<QString,QString> row;
                    row["colId"] = q.value("colId").toString();
                    row["rowId"] = q.value("rowId").toString();
                    row["value"] = q.value("value").toString();
                    oldRows.append(row);
                }

                q.exec("DROP TABLE datapoints");
                createTable("datapoints",tables()["datapoints"][0],db);
                for (const auto &row : oldRows){
                    q.exec("INSERT INTO datapoints (colId,rowId,value) VALUES "
                           "(" + row["colId"] +
                           "," + row["rowId"] +
                           "," + row["value"] +
                           ")");
                    evaluateQuery(q,"migrate_v1_1 - insert into new table \"datapoints\"");
                }
                printInfo("table \"datapoints\" migrated.");
            }
        }
    }

    QSqlDatabase::removeDatabase(name());
}
