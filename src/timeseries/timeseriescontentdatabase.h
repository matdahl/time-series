/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <MDTK/db/abstractdatabaseclient.h>

#include "datacolumn.h"
#include "datarow.h"

class TimeSeriesContentDatabase : public MDTK::AbstractDatabaseClient
{
public:
    typedef QMap<uint,QPair<QDate,QMap<uint,qreal>>> RowDataMap;

    TimeSeriesContentDatabase(const uint tsUid);
    ~TimeSeriesContentDatabase();

    // AbstractDatabaseClient interface
    QString name() const {return m_db_name;}
    TableMap tables() const {
        TableMap tables;
        tables["columns"] = {
            { // < v1.1
                {"idx",  ColumnType::INTEGER},
                {"type", ColumnType::INTEGER},
                {"title",ColumnType::TEXT},
                {"unit", ColumnType::TEXT},
                {"usePreviousAsDefault",ColumnType::BOOLEAN},
                {"defaultValue",ColumnType::REAL},
                {"allowNegative", ColumnType::BOOLEAN},
                {"digits",ColumnType::INTEGER,"1"},
                {"precision",ColumnType::INTEGER},
                {"baseCol1",ColumnType::INTEGER},
                {"baseCol2",ColumnType::INTEGER},
                {"referenceTime",ColumnType::INTEGER}
            },
            { // >= v1.1
                {"idx",  ColumnType::INTEGER},
                {"type", ColumnType::INTEGER},
                {"title",ColumnType::TEXT},
                {"unit", ColumnType::TEXT},
                {"usePreviousAsDefault",ColumnType::BOOLEAN},
                {"defaultValue",ColumnType::REAL},
                {"allowNegative", ColumnType::BOOLEAN},
                {"digits",ColumnType::INTEGER,"1"},
                {"precision",ColumnType::INTEGER},
                {"baseCol1",ColumnType::INTEGER},
                {"baseCol2",ColumnType::INTEGER},
                {"referenceTime",ColumnType::INTEGER},
                {"scale1",ColumnType::INTEGER,"1"},
                {"scale2",ColumnType::INTEGER,"1"}
            }
        };
        tables["rows"] = {
            {
                {"date",ColumnType::DATE}
            }
        };
        tables["datapoints"] = {
            {
                {"colId",ColumnType::INTEGER},
                {"rowId",ColumnType::INTEGER},
                {"value",ColumnType::REAL}
            }
        };
        return tables;
    }

protected:
    void handleMigrations(TableMigrationEvent &event);

public:
    DataColumnList readColumns(QObject *parent = nullptr);
    bool insertColumn(DataColumn *col);
    bool updateColumn(DataColumn *col);
    bool removeColumn(const uint uid);

    // returns a map {rowId : {colId : value } }
    RowDataMap readDataPoints();
    bool insertDataRow(DataRow *row);
    bool removeDataRow(DataRow *row);
    bool updateValue(DataColumn *col, DataRow *row, const qreal value);

private:
    const QString m_db_name;

    void migrate_v1_1();
};
