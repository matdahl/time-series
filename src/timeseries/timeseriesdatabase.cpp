/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseriesdatabase.h"

bool TimeseriesDatabase::read(TimeseriesList &list){
    QSqlDatabase db = openDatabase();
    QSqlQuery q = selectAll("timeseries",db,"rank");
    if (!q.isActive())
        return false;

    while (q.next()){
        try{
            const uint    uid   = parseUInt(q,"uid");
            const uint    rank  = parseInteger(q,"rank");
            const QString title = parseText(q,"title");

            Timeseries *ts = new Timeseries;
            ts->setUid(uid);
            ts->setRank(rank);
            ts->setTitle(title);
            list.push_back(ts);
        } catch (std::exception e) {
            printError("TimeSeriesDatabase::readTimeSeries(): could not parse a time series: "+QString(e.what())+" - Ignore and continue with next.");
        }
    }
    return true;
}

bool TimeseriesDatabase::insert(Timeseries *ts){
    QSqlDatabase db = openDatabase();
    if (!db.isOpen())
        return false;

    // update ranks
    QSqlQuery q("UPDATE timeseries SET rank=rank+1",db);
    const bool success = evaluateQuery(q,"insert()");
    if (!success)
        return false;

    const QMap<QString,QString> values = {
        {"rank",writeInteger(1)},
        {"title",writeText(ts->title())}
    };
    const int insertIdx = simpleInsert("timeseries",values);
    if (insertIdx >= 0){
        ts->setUid(insertIdx);
        return true;
    }
    return false;
}

bool TimeseriesDatabase::update(const Timeseries *ts){
    const QMap<QString,QString> values = {
        {"rank",writeInteger(ts->rank())},
        {"title",writeText(ts->title())}
    };
    return simpleUpdate("timeseries",values,writeUInt(ts->uid()));
}

bool TimeseriesDatabase::remove(const uint uid){
    return simpleDelete("timeseries",writeUInt(uid));
}
