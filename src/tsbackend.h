/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QDebug>
#include <QDateTime>

#include "timeseries/timeseriesmanager.h"
#include "export/exportmanager.h"

class TSBackend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TimeseriesManager *timeseries READ timeseries CONSTANT)
    Q_PROPERTY(ExportManager     *exports    READ exports    CONSTANT)

public:
    explicit TSBackend(QObject *parent = nullptr);

    // QML properties - getter
    TimeseriesManager *timeseries() const {return m_timeseries;}
    ExportManager     *exports()    const {return m_exports;}

private:
    // QML properties - members
    TimeseriesManager *m_timeseries = nullptr;
    ExportManager     *m_exports    = nullptr;

};
