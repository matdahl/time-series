# Time Series

This app is designed to easily record time series data on your phone.
The data can be exported as CSV files to further process them anywhere else.

## Translations

Currently, the app is available in the following languages:

| Language   | Translators | Status |
| ---------- | ----------- | ------ |
| Dutch      | @vistaus    | ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F56591315%2Fjobs%2Fartifacts%2Fxenial%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.nl.progress&label=xenial) ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F56591315%2Fjobs%2Fartifacts%2Ffocal%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.nl.progress&label=focal)  |
| English    | @matdahl    | default language |
| German     | @matdahl    | ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F56591315%2Fjobs%2Fartifacts%2Fxenial%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.de.progress&label=xenial) ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F56591315%2Fjobs%2Fartifacts%2Ffocal%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.de.progress&label=focal) |

Additional translations are very welcome and can be created by loading the file
**po/time-series.matdahl.pot** with a translation tool like [Poedit](https://poedit.net/)
and adding the resulting .po file to the po directory of this repository.

## License

Copyright (C) 2024  Matthias Dahlmanns

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
