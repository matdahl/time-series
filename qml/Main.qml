/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Qt.labs.settings 1.1
import Lomiri.Components 1.3
import MDTK 1.0 as MDTK
import MDTK.Manual 1.0 as MDTKManual

import "dialogs"

import TSCore 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'time-series.matdahl'
    automaticOrientation: true
    anchorToKeyboard: true

    width: units.gu(45)
    height: units.gu(75)

    /* ----- theming ----- */
    MDTK.Colors{
        id: colors
        defaultIndex: 2
    }
    theme.name: colors.themeName
    backgroundColor: colors.currentBackground

    /* ----- theming ----- */
    Settings{
        id: settings
        property bool showBottomEdgeHint: false
    }

    /* ----- initialisation ----- */
    Component.onCompleted: {
        pageStack.push(Qt.resolvedUrl("qrc:/qml/pages/StartPage.qml"))
    }

    /* ----- content components ----- */
    PageStack{
        id: pageStack
    }

    BottomEdgeTimeseriesUpdate{
        id: bottomEdgeEditTimeseries
    }

    BottomEdgeTimeseriesNew{
        id: bottomEdgeNewTimeseries
    }

    BottomEdgeDataRowNew{
        id: bottomEdgeNewDataRow
        enabled: TSBackend.timeseries.model.count > 0
    }

    DialogTimeseriesDelete{
        id: dialogDeleteTimeseries
    }

    MDTKManual.Manual{
        id: manual
        dialogComponent: "qrc:/qml/dialogs/TSManualDialog.qml"
    }
}
