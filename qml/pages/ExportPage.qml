/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import MDTK 1.0
import MDTK.Exports 1.0

import TSCore 1.0

ThemedFlickablePage {
    id: root
    // TRANSLATORS: %1 refers to the italic title of the timeseries to export
    title: i18n.tr("Export %1").arg("<i>%1</i>").arg(TSBackend.exports.timeseries.title)

    padding: units.gu(2)
    spacing: units.gu(2)

    FilenameField{
        id: inputFilename
        width: root.contentWidth
        fileExtension: "csv"
        defaultFilename: Qt.formatDate(new Date(),"yyyy-MM-dd") + "-" + TSBackend.exports.timeseries.title.replace(" ","_")
    }

    DelimiterSelector{
        id: inputDelimiter
        width: root.contentWidth
        Component.onCompleted: selectDelimiter(TSBackend.exports.delimiter)
        onSelectedDelimiterChanged: TSBackend.exports.delimiter = selectedDelimiter
    }

    Label{
        text: i18n.tr("Date format")
    }
    TextField{
        id: inputDateFormat
        width: root.contentWidth
        text: TSBackend.exports.dateFormat
        onTextChanged: TSBackend.exports.dateFormat = text
        inputMethodHints: Qt.ImhNoPredictiveText
        hasClearButton: false
    }

    OptionSelector{
        id: inputDerivedColumnsMode
        text: i18n.tr("Derived columns")
        width: root.contentWidth
        expanded: true
        model: [
            i18n.tr("Export values"),
            i18n.tr("Export as references")
        ]
        selectedIndex: TSBackend.exports.resolveReferences ? 0 : 1
        onSelectedIndexChanged: TSBackend.exports.resolveReferences = (selectedIndex === 0)
    }

    Button{
        anchors.horizontalCenter: parent.horizontalCenter
        width: 0.6*root.contentWidth
        color: LomiriColors.orange
        // TRANSLATORS: the imperative of the verb "to export"
        text: i18n.tr("Export")
        onClicked: {
            TSBackend.exports.generateExportFile(inputFilename.filename)
            pageStack.push(Qt.resolvedUrl("qrc:/qml/pages/ExportSelectPage.qml"))
        }
    }
}
