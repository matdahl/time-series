/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

import TSCore 1.0

ListItem{
    id: root

    property var dragParent

    leadingActions: ListItemActions{
        actions: [
            Action{
                iconName: "delete"
                name: i18n.tr("Delete")
                onTriggered: dialogDeleteTimeseries.show(timeseries)
            }
        ]
    }

    trailingActions: ListItemActions{
        actions: [
            Action{
                iconName: "edit"
                name: i18n.tr("Edit")
                onTriggered: bottomEdgeEditTimeseries.show(timeseries)
            },
            Action{
                iconName: "share"
                name: i18n.tr("Share")
                onTriggered: {
                    TSBackend.exports.timeseries = timeseries
                    pageStack.push(Qt.resolvedUrl("qrc:/qml/pages/ExportPage.qml"))
                }
            }
        ]
    }

    onClicked: {
        TSBackend.timeseries.selected = timeseries
        pageStack.push(Qt.resolvedUrl("qrc:/qml/pages/TimeseriesPage.qml"))
    }

    Rectangle{
        anchors.fill: parent
        color: theme.palette.normal.base
        opacity: index%2 === 0 ? 0.13 : 0.1
    }

    ListItemLayout{
        id: layout
        padding.top: units.gu(1.5)

        Rectangle{
            SlotsLayout.position: SlotsLayout.First
            width: units.gu(4)
            height: units.gu(4)
            radius: units.gu(1)
            color: colors.currentHeader

            Icon{
                anchors.centerIn: parent
                height: units.gu(3)
                name: "grip-large"
                color: theme.palette.normal.backgroundText
            }

            MouseArea{
                id: dragMouse
                anchors.centerIn: parent
                height: root.height
                width: height
                drag.target: layout
            }
        }

        title.text: timeseries.title

        Icon{
            SlotsLayout.position: SlotsLayout.Last
            height: units.gu(3)
            name: "next"
            color: theme.palette.normal.backgroundText
        }

        property int dragItemIndex: index

        states: [
            State {
                when: layout.Drag.active
                ParentChange {
                    target: layout
                    parent: root.dragParent
                }
            },
            State {
                when: !layout.Drag.active
                AnchorChanges {
                    target: layout
                    anchors.horizontalCenter: layout.parent.horizontalCenter
                    anchors.verticalCenter: layout.parent.verticalCenter
                }
            }
        ]
        Drag.active: dragMouse.drag.active
        Drag.hotSpot.x: layout.width / 2
        Drag.hotSpot.y: layout.height / 2
    }

    DropArea{
        anchors.fill: parent
        onEntered: TSBackend.timeseries.model.swap(drag.source.dragItemIndex,index)
    }
}
