/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Item {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    height: childrenRect.height

    property alias text: title.text
    property alias model: inputColumn.model
    property alias canEditColumn: inputColumn.enabled

    property real expansionCount: 2.5
    property real scaleFieldWidth: units.gu(10)

    readonly property int   selectedColumn: canEditColumn ? inputColumn.model.idxAt(inputColumn.selectedIndex) : 0
    readonly property alias selectedIndex: inputColumn.selectedIndex
    readonly property alias scale: inputScale.value

    function avoidIndex(idx){
        if (selectedIndex === idx && model.count > 1){
            if (selectedIndex > 0)
                inputColumn.selectedIndex -= 1
            else
                inputColumn.selectedIndex += 1
        }
    }

    function setScale(scale){
        inputScale.text = scale
    }

    Label{
        id: title
    }

    TextField{
        id: inputScale
        anchors{
            top: title.bottom
            topMargin: units.gu(2)
        }
        width: root.scaleFieldWidth
        text: "1"
        hasClearButton: false
        validator: DoubleValidator{}
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        readonly property real value: {
            const val = parseFloat(displayText)
            return val === val ? val : 1.0
        }
    }

    OptionSelector{
        id: inputColumn
        anchors{
            top: inputScale.top
            left: inputScale.right
            right: parent.right
            leftMargin: units.gu(2)
        }
        expanded: true
        containerHeight: Math.min(root.expansionCount,model.count)*itemHeight
        delegate: OptionSelectorDelegate{
            text: dataColumn ? dataColumn.title : modelData
        }
    }
}
