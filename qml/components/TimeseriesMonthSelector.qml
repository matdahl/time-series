/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program  is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Item {
    id: root
    width: parent.width
    height: units.gu(6)

    property date month: new Date()

    property bool canGoFirst: true
    property bool canGoLast: true

    signal monthSelected(date month)
    signal goFirst()
    signal goLast()

    property int buttonWidth: units.gu(6)

    MouseArea{
        id: mouseFirst
        anchors{
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: root.buttonWidth
        enabled: root.canGoFirst

        Rectangle{
            anchors.fill: parent
            color: theme.palette[root.canGoFirst ? "normal" : "disabled"].base
            opacity: 0.4
        }

        Icon{
            anchors.centerIn: parent
            name: "go-first"
            height: units.gu(3)
            color: theme.palette[root.canGoFirst ? "normal" : "disabled"].backgroundText
            rotation: 90
        }

        onClicked: root.goFirst()
    }

    MouseArea{
        anchors{
            top: parent.top
            left: mouseFirst.right
            bottom: parent.bottom
        }
        width: root.buttonWidth
        enabled: root.canGoFirst

        Rectangle{
            anchors.fill: parent
            color: theme.palette[root.canGoFirst ? "normal" : "disabled"].base
            opacity: 0.2
        }

        Icon{
            anchors.centerIn: parent
            name: "go-previous"
            height: units.gu(3)
            color: theme.palette[root.canGoFirst ? "normal" : "disabled"].backgroundText
            rotation: 90
        }

        onClicked: root.monthSelected(new Date(root.month.getFullYear(),root.month.getMonth()-1,2))
    }

    Label{
        anchors.centerIn: parent
        textSize: Label.Large
        font.bold: true
        text: Qt.formatDate(root.month,"MMMM yyyy")
    }

    MouseArea{
        anchors{
            top: parent.top
            right: mouseLast.left
            bottom: parent.bottom
        }
        width: root.buttonWidth
        enabled: root.canGoLast

        Rectangle{
            anchors.fill: parent
            color: theme.palette[root.canGoLast ? "normal" : "disabled"].base
            opacity: 0.2
        }

        Icon{
            anchors.centerIn: parent
            name: "go-next"
            height: units.gu(3)
            color: theme.palette[root.canGoLast ? "normal" : "disabled"].backgroundText
            rotation: 90
        }

        onClicked: root.monthSelected(new Date(root.month.getFullYear(),root.month.getMonth()+1,2))
    }

    MouseArea{
        id: mouseLast
        anchors{
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }
        width: root.buttonWidth
        enabled: root.canGoLast

        Rectangle{
            anchors.fill: parent
            color: theme.palette[root.canGoLast ? "normal" : "disabled"].base
            opacity: 0.4
        }

        Icon{
            anchors.centerIn: parent
            name: "go-last"
            height: units.gu(3)
            color: theme.palette[root.canGoLast ? "normal" : "disabled"].backgroundText
            rotation: 90
        }

        onClicked: root.goLast()
    }
}
