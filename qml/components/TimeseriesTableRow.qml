/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

import TSCore 1.0

Item{
    id: root
    height: table.rowHeight
    width: table.width
    x: deleteAreaRevealed ? deleteAreaWidth : 0

    property bool deleteAreaRevealed: false
    property int  deleteAreaWidth: units.gu(7)

    readonly property real headerContentX: headerCells.contentX
    onHeaderContentXChanged: {
        if (deleteAreaRevealed && !flickable.moving)
            deleteAreaRevealed = false
    }

    Item{
        id: content
        height: root.height
        width: root.width
        x: flickable.moving ? Math.max(0,-flickable.horizontalOvershoot) : 0
        z: 1

        MouseArea{
            id: deleteArea
            anchors.right: parent.left
            height: parent.height
            width: root.deleteAreaWidth

            Rectangle{
                anchors.fill: parent
                color: theme.palette[deleteArea.pressed ? "highlighted" : "normal"].foreground
            }

            Icon{
                anchors.centerIn: parent
                height: units.gu(2)
                name: "delete"
                color: theme.palette.normal.negative
            }

            onClicked: ts.removeRow(modelRow)
        }

        TimeseriesTableCell{
            id: dataDateCell
            width:  table.ts.dataColumnWidth
            height: parent.height
            backgroundColor: table.headerColor
            backgroundOpacity: table.backgroundOpacityDate + (rowIndex%2 === 0 ? 0.05 : 0)

            Column{
                anchors.centerIn: parent
                width: Math.max(lbDate.width,lbYear.width)
                onWidthChanged: {
                    if (table.ts.dataColumnWidth < width + units.gu(4))
                        table.ts.dataColumnWidth = width + units.gu(4)
                }

                Label{
                    id: lbDate
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: Qt.formatDate(modelRow.date,"d MMM")
                }

                Label{
                    id: lbYear
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: Qt.formatDate(modelRow.date,"yyyy")
                    textSize: Label.Small
                }
            }
        }
    }

    Flickable{
        id: flickable
        x: dataDateCell.width
        width: root.width - x
        height: table.rowHeight
        contentWidth: Math.max(width+root.deleteAreaWidth,columnsRow.width)

        onDragEnded: {
            const newRevealed = !root.deleteAreaRevealed && (contentX < -root.deleteAreaWidth/3)
            if (newRevealed === root.deleteAreaRevealed)
                return

            root.deleteAreaRevealed = newRevealed
            if (newRevealed)
                contentX = Math.min(0,contentX+root.deleteAreaWidth)
            else
                contentX -= root.deleteAreaWidth
        }

        Row{
            id: columnsRow
            height: table.rowHeight
            readonly property int rowIdx: rowIndex

            Repeater{
                model: table.ts.columns
                delegate: TimeseriesTableCell{
                    id: dataCell
                    height: table.rowHeight
                    width: dataColumn.width

                    readonly property var dataCol: dataColumn
                    backgroundColor: theme.palette.normal.base
                    backgroundOpacity: (dataColumn.type !== DataColumn.RAWDATA ? 0.2 : 0.1)
                                     + (columnsRow.rowIdx%2 === 0 ? 0.1 : 0.0)

                    Row{
                        id: labelRow
                        anchors{
                            verticalCenter: parent.verticalCenter
                            right: parent.right
                            rightMargin: units.gu(1)
                        }
                        height: units.gu(2)
                        onWidthChanged: {
                            if (dataColumn.width < width + units.gu(2))
                                dataColumn.width = width + units.gu(2)
                        }

                        Label{
                            id: lbValue
                            anchors. verticalCenter: parent.verticalCenter
                            text: (modelRow.contains(dataColumn.uid) ? dataColumn.formatValue(modelRow.valueAt(dataColumn.uid)) : "-")
                                + modelValueChanges // bind property to refresh when a value has changed
                        }

                        Label{
                            id: lbUnit
                            anchors. verticalCenter: parent.verticalCenter
                            color: theme.palette.normal.base
                            text: (modelRow.contains(dataColumn.uid) && dataColumn.unit.length>0 ? " "+dataColumn.unit : "")
                                + modelValueChanges // bind property to refresh when a value has changed
                        }
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: valueEditPopover.open(this)
                        enabled: dataCell.dataCol.type === DataColumn.RAWDATA
                    }

                    DataColumnValueEditPopover{
                        id: valueEditPopover
                        dataColumn: dataCell.dataCol
                        value: dataCell.dataCol.type === DataColumn.RAWDATA ? modelRow.valueAt(dataCell.dataCol.uid) : 0
                        onValueChanged: {
                            if (isOpen && dataCell.dataCol.type === DataColumn.RAWDATA)
                                table.ts.updateValue(dataColumn,modelRow,value)
                        }
                    } // valueEditPopover
                }
            }
        } // columnsRow
    }

    Binding{
        target: flickable
        property: "contentX"
        value: headerCells.contentX
        when: !flickable.moving
    }

    Binding{
        target: headerCells
        property: "contentX"
        value: Math.max(0,flickable.contentX)
        when: flickable.moving
    }
}
