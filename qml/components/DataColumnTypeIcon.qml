/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

import "qrc:/utils/ColumnUtils.js" as Utils

import TSCore 1.0

Loader {
    id: root
    height: units.gu(4)

    readonly property string iconName: {
        switch(dataColumn.type){
        case DataColumn.RAWDATA:
            return ""
        case DataColumn.AGGREGATION:
            return "sum"
        case DataColumn.GRADIENT:
            return "gradient"
        case DataColumn.AVERAGE:
            return "sum"
        case DataColumn.SUM:
            return "add"
        case DataColumn.DIFFERENCE:
            return "subtract"
        case DataColumn.PRODUCT:
            return "multiply"
        case DataColumn.QUOTIENT:
            return "divide"
        }
    }

    sourceComponent: {
        switch(dataColumn.type){
        case DataColumn.RAWDATA:
            return componentRawData
        case DataColumn.AGGREGATION:
            return componentAggregation
        case DataColumn.GRADIENT:
            return componentGradient
        case DataColumn.AVERAGE:
            return componentAverage
        case DataColumn.SUM:
        case DataColumn.DIFFERENCE:
        case DataColumn.PRODUCT:
        case DataColumn.QUOTIENT:
            return componentBinary
        }
    }

    Component{
        id: componentRawData
        Item{
            Label{
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                }
                text: Utils.columnTitle(DataColumn.RAWDATA)
                font.italic: true
            }
        }
    }

    Component{
        id: componentAggregation
        Row{
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
            }
            spacing: units.gu(0.5)
            height: units.gu(3)

            Label{
                anchors.verticalCenter: parent.verticalCenter
                textSize: Label.Small
                text: dataColumn.scale1
                visible: dataColumn.scale1 !== 1.0
            }

            Icon{
                anchors.verticalCenter: parent.verticalCenter
                height: units.gu(3)
                source: "image://icons/sum"
                color: theme.palette.normal.backgroundSecondaryText
            }

            DataColumnIndexIcon{
                anchors.verticalCenter: parent.verticalCenter
                size: units.gu(2.5)
                text: dataColumn.baseColumn1
            }
        }
    }

    Component{
        id: componentGradient
        Row{
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
            }
            spacing: units.gu(0.5)

            Label{
                anchors.verticalCenter: parent.verticalCenter
                textSize: Label.Small
                text: dataColumn.scale1
                visible: dataColumn.scale1 !== 1.0
            }

            Icon{
                anchors.verticalCenter: parent.verticalCenter
                height: units.gu(3)
                source: "image://icons/gradient"
                color: theme.palette.normal.backgroundSecondaryText
            }

            DataColumnIndexIcon{
                anchors.verticalCenter: parent.verticalCenter
                size: units.gu(2.5)
                text: dataColumn.baseColumn1
            }
        }

    }

    Component{
        id: componentAverage
        Row{
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
            }
            spacing: units.gu(0.5)
            height: units.gu(3)

            Label{
                anchors.verticalCenter: parent.verticalCenter
                textSize: Label.Small
                text: dataColumn.scale1
                visible: dataColumn.scale1 !== 1.0
            }


            DataColumnIndexIcon{
                anchors.verticalCenter: parent.verticalCenter
                size: units.gu(2.5)
                text: dataColumn.baseColumn1

                Rectangle{
                    anchors{
                        horizontalCenter: parent.horizontalCenter
                        bottom: parent.top
                        bottomMargin: units.gu(0.5)
                    }
                    width: parent.width + units.gu(1)
                    height: units.gu(0.25)
                    color: theme.palette.normal.backgroundSecondaryText
                }
            }
        }
    }

    Component{
        id: componentBinary
        Row{
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
            }
            spacing: units.gu(0.5)

            Label{
                anchors.verticalCenter: parent.verticalCenter
                textSize: Label.Small
                text: dataColumn.scale1
                visible: dataColumn.scale1 !== 1.0
            }

            DataColumnIndexIcon{
                anchors.verticalCenter: parent.verticalCenter
                size: units.gu(2.5)
                text: dataColumn.baseColumn1
            }

            Icon{
                anchors.verticalCenter: parent.verticalCenter
                width: units.gu(2)
                color: theme.palette.normal.backgroundSecondaryText
                source: "image://icons/"+iconName
            }

            Label{
                anchors.verticalCenter: parent.verticalCenter
                textSize: Label.Small
                text: dataColumn.scale2
                visible: dataColumn.scale2 !== 1.0
            }

            DataColumnIndexIcon{
                anchors.verticalCenter: parent.verticalCenter
                size: units.gu(2.5)
                text: dataColumn.baseColumn2
            }
        }

    }
}
