/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Qt.labs.settings 1.1

import TSCore 1.0


Rectangle{
    id: root
    width: parent.width
    height: sections.height
    color: colors.currentBackground

    property var model: TSBackend.timeseries.model
    property alias selectedIndex: sections.selectedIndex

    readonly property var selectedTimeseries: model.at(sections.selectedIndex)
    onSelectedTimeseriesChanged: tsSettings.selectedTimeseries = selectedTimeseries ? selectedTimeseries.uid : 0

    property var tsSettings: Settings{
        category: "TimeseriesSelector"
        property int selectedTimeseries: 0
    }

    Sections{
        id: sections
        width: root.width
        model: []
    }

    function refreshTitles(){
        const selectedTS = root.selectedTimeseries ? root.selectedTimeseries.uid : 0
        sections.model = model.titles()
        sections.selectedIndex = Math.max(model.indexOf(selectedTS),0)
    }

    Component.onCompleted: {
        sections.selectedIndex = Math.max(model.indexOf(tsSettings.selectedTimeseries),0)
    }

    readonly property int counts: model.count
    onCountsChanged: refreshTitles()

    Item{
        id: titleItems
        Repeater{
            model: root.model
            delegate: Item{
                // react to renaming of timeseries
                readonly property string title: timeseries.title
                onTitleChanged: root.refreshTitles()

                // react to reordering of timeseries
                readonly property int idx: index
                onIdxChanged:   root.refreshTitles()
            }
        }
    }
}
