/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

import TSCore 1.0

Item {
    id: table

    property var ts: TSBackend.timeseries.selected

    property color headerColor: colors.indexColor > 0 ? colors.currentHeader : theme.palette.normal.foreground
    property int   headerHeight: units.gu(6)
    property int   rowHeight: units.gu(5)

    property real backgroundOpacityHeader: 0.6
    property real backgroundOpacityDate:   0.4

    readonly property int lowestVisibleIndex:  Math.floor(dataRows.contentY/rowHeight)
    readonly property int highestVisibleIndex: lowestVisibleIndex + Math.ceil(dataRows.height/rowHeight)
    readonly property int contentHeight: (ts.rows.count+2)*rowHeight

    readonly property bool isAtTop: dataRows.contentY <= 0
    readonly property bool isAtEnd: dataRows.contentY >= contentHeight - dataRows.height

    Component.onCompleted: toEnd()

    function bringIndexToTop(idx){
        dataRows.contentY = Math.min(contentHeight - dataRows.height,idx*rowHeight)
    }

    function toTop(){
        dataRows.contentY = 0
    }

    function toEnd(){
        dataRows.contentY = Math.max(0,contentHeight - dataRows.height)
    }


    TimeseriesTableCell {
        id: dateHeaderCell
        height: headerHeight
        width: ts.dataColumnWidth
        backgroundColor: table.headerColor
        backgroundOpacity: table.backgroundOpacityHeader

        Label{
            anchors.verticalCenter: parent.verticalCenter
            x: units.gu(2)
            font.bold: true
            text: i18n.tr("Date")
            onWidthChanged: {
                if (ts.dataColumnWidth < width + units.gu(4))
                    ts.dataColumnWidth = width + units.gu(4)
            }
        }
    }

    Flickable{
        id: headerCells
        anchors{
            left: dateHeaderCell.right
            right: parent.right
        }
        height: table.headerHeight
        clip: true

        contentWidth: Math.max(width,headerCellsRow.width + units.gu(2))
        onContentXChanged: {
            if (headerCellsRow.width < width && contentX > 0)
                contentX = 0
        }

        Row{
            id: headerCellsRow
            height: table.headerHeight

            Repeater{
                model: table.ts.columns
                delegate: TimeseriesTableCell{
                    height: table.headerHeight
                    width: dataColumn.width
                    backgroundColor: table.headerColor
                    backgroundOpacity: table.backgroundOpacityHeader

                    Label{
                        anchors.centerIn: parent
                        font.bold: true
                        text: dataColumn.title
                        onWidthChanged: {
                            if (dataColumn.width < width + units.gu(2))
                                dataColumn.width = width + units.gu(2)
                        }
                    }
                }
            }
        }
    }

    Flickable{
        id: dataRows
        anchors{
            top: dateHeaderCell.bottom
            bottom: parent.bottom
        }
        clip: true
        width: table.width
        contentHeight: Math.max(table.contentHeight,height)

        Repeater{
            id: repeaterRows
            model: ts.rows
            delegate: Loader{
                y: index * table.rowHeight
                active: index >= table.lowestVisibleIndex-1 &&
                        index <= table.highestVisibleIndex+1
                property int rowIndex: index
                source: "TimeseriesTableRow.qml"
            } // delegate
        } // repeaterRows

        Button{
            anchors.horizontalCenter: parent.horizontalCenter
            y: ts.rows.count*table.rowHeight + units.gu(1)
            width: 0.6* table.width
            height: table.rowHeight - units.gu(1)
            color: theme.palette.normal.positive
            Icon{
                anchors.centerIn: parent
                height: parent.height-units.gu(1)
                name:"add"
                color: theme.palette.normal.positiveText
            }
            onClicked: bottomEdgeNewDataRow.show(ts.uid)
        }
    } // dataRows
} // table
