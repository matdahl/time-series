/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Rectangle{
    property int size: units.gu(4)
    width:  size
    height: size
    radius: units.gu(0.5)
    color: theme.palette.normal.base

    property alias text:      label.text
    property alias textSize:  label.textSize
    property alias textBold:  label.font.bold
    property alias textColor: label.color

    Label{
        id: label
        anchors.centerIn: parent
        color: theme.palette.normal.baseText
        textSize: root.size >= units.gu(3) ? Label.Normal : Label.Small
    }
}
