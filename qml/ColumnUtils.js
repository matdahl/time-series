/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

.import TSCore 1.0 as TSCore

function columnTitle(columnType){
    switch (columnType) {
    case TSCore.DataColumn.RAWDATA:
        // TRANSLATORS: title of the column type for manually entered data
        return i18n.tr("Manual data")
    case TSCore.DataColumn.AGGREGATION:
        // TRANSLATORS: title of the column type that aggregates all previous values of an other column
        return i18n.tr("Aggregation")
    case TSCore.DataColumn.SUM:
        // TRANSLATORS: title of the column type for sum of two other columns
        return i18n.tr("Sum")
    case TSCore.DataColumn.DIFFERENCE:
        // TRANSLATORS: title of the column type for difference of two other columns
        return i18n.tr("Difference")
    case TSCore.DataColumn.PRODUCT:
        // TRANSLATORS: title of the column type for product of two other columns
        return i18n.tr("Product")
    case TSCore.DataColumn.QUOTIENT:
        // TRANSLATORS: title of the column type for quotient of two other columns
        return i18n.tr("Quotient")
    case TSCore.DataColumn.GRADIENT:
        // TRANSLATORS: title of the column type for gradient of an other column
        return i18n.tr("Gradient")
    case TSCore.DataColumn.AVERAGE:
        // TRANSLATORS: title of the column type for the average value of an other column
        return i18n.tr("Average")
    }
    return "UNKNOWN"
}
