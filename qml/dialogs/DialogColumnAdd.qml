/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import "qrc:/utils/ColumnUtils.js" as Utils
import "qrc:/qml/components"

import TSCore 1.0

Item {
    id: root

    property var dataColumnModel

    function show(){
        PopupUtils.open(dialogComponent)
    }

    readonly property var modelZeroColumns: [
        DataColumn.RAWDATA
    ]

    readonly property var modelOneColumn: [
        DataColumn.RAWDATA,
        DataColumn.AGGREGATION,
        DataColumn.GRADIENT,
        DataColumn.AVERAGE
    ]

    readonly property var modelAllColumns: [
        DataColumn.RAWDATA,
        DataColumn.AGGREGATION,
        DataColumn.GRADIENT,
        DataColumn.AVERAGE,
        DataColumn.SUM,
        DataColumn.DIFFERENCE,
        DataColumn.PRODUCT,
        DataColumn.QUOTIENT
    ]

    Component{
        id: dialogComponent
        Dialog{
            id: dialog

            property var dataColumn: DataColumn{}

            // flag to highlight, that the column is frozen
            property bool freeze: false

            property int requiredBaseColumns: {
                switch (dataColumn.type){
                case DataColumn.AGGREGATION:
                case DataColumn.GRADIENT:
                case DataColumn.AVERAGE:
                    return 1
                case DataColumn.SUM:
                case DataColumn.DIFFERENCE:
                case DataColumn.PRODUCT:
                case DataColumn.QUOTIENT:
                    return 2
                }
                return 0
            }

            title: i18n.tr("Add column")

            TextField{
                id: inputTitle
                placeholderText: i18n.tr("insert column name ...")
                onDisplayTextChanged:{
                    if (!dialog.freeze)
                        dialog.dataColumn.title = displayText
                }
            }

            OptionSelector{
                text: i18n.tr("Column type")
                model: {
                    if (root.dataColumnModel.count > 1)
                        return root.modelAllColumns
                    if (root.dataColumnModel.count > 0)
                        return root.modelOneColumn
                    return root.modelZeroColumns
                }
                expanded: true
                containerHeight: Math.min(3.5,model.length)*itemHeight
                onSelectedIndexChanged:{
                    if (!dialog.freeze)
                        dialog.dataColumn.type = model[selectedIndex]
                }
                delegate: OptionSelectorDelegate{
                    text: Utils.columnTitle(modelData)
                }
            }

            ColumnValueVisualisationEdit{
                dataColumn: dialog.dataColumn
                freeze: dialog.freeze
            }

            /* ------ Default value selection ----- */
            OptionSelector{
                id: inputDefaultValueMode
                text: i18n.tr("Default value")
                visible: dialog.dataColumn.type === DataColumn.RAWDATA
                model: [
                    i18n.tr("Use previous value"),
                    i18n.tr("Manual value")
                ]
                expanded: true
            }
            DataColumnValueEditButton{
                id: inputDefaultValue
                visible: dialog.dataColumn.type === DataColumn.RAWDATA && inputDefaultValueMode.selectedIndex === 1
                dataCol: dialog.dataColumn
                value: dataColumn.defaultValue
                onValueChanged: dataColumn.defaultValue = value
            }
            /* ------------------------------------ */

            /* ------ Reference selection ----- */
            ReferenceColumnSelector{
                id: inputFirstColumn
                text: i18n.tr("First base column")
                visible: dialog.requiredBaseColumns > 0
                model: root.dataColumnModel
                onSelectedIndexChanged: inputSecondColumn.avoidIndex(selectedIndex)
            }

            ReferenceColumnSelector{
                id: inputSecondColumn
                text: i18n.tr("Second base column")
                visible: dialog.requiredBaseColumns > 1
                model: root.dataColumnModel
                Component.onCompleted: avoidIndex(inputFirstColumn.selectedIndex)
            }
            /* -------------------------------- */

            /* ------ Timerange selection ----- */
            readonly property bool needTimerangeSelection: dataColumn.type === DataColumn.AGGREGATION ||
                                                           dataColumn.type === DataColumn.AVERAGE ||
                                                           dataColumn.type === DataColumn.GRADIENT

            OptionSelector{
                id: inputTimerangeMode
                visible: dialog.needTimerangeSelection
                text: {
                    switch (dataColumn.type){
                    case DataColumn.AGGREGATION:
                        return i18n.tr("Aggregation time interval")
                    case DataColumn.AVERAGE:
                        return i18n.tr("Averaging time interval")
                    case DataColumn.GRADIENT:
                        return i18n.tr("Gradient type (change from last row)")
                    }
                }

                model: dataColumn.type === DataColumn.GRADIENT ? 2 : 4
                function delegateTitle(index){
                    switch (dataColumn.type) {
                    case DataColumn.AGGREGATION:
                    case DataColumn.AVERAGE:
                        switch (index) {
                        case 0: return i18n.tr("all")
                        case 1: return i18n.tr("last week")
                        case 2: return i18n.tr("last 30 days")
                        case 3: return i18n.tr("manual")
                        }
                        break
                    case DataColumn.GRADIENT:
                        switch (index) {
                        case 0: return i18n.tr("absolute change")
                        case 1: return i18n.tr("change per day")
                        }
                        break
                    }
                    return ""
                }
                delegate: OptionSelectorDelegate{
                    text: inputTimerangeMode.delegateTitle(index)
                }
                readonly property bool isManual: selectedIndex === 3
            }
            Slider{
                id: inputTimerangeValue
                visible: dialog.needTimerangeSelection && inputTimerangeMode.isManual
                minimumValue: 1
                maximumValue: 30
                value: 7
            }


            /* -------------------------------- */

            Rectangle{
                width: dialog.contentWidth
                height: units.gu(0.25)
                color: theme.palette.normal.foreground
            }

            Button{
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }
            Button{
                text: i18n.tr("Add")
                color: LomiriColors.orange
                enabled: inputTitle.text.length > 0
                onClicked:{
                    // set remaining properties of column
                    if (dialog.requiredBaseColumns > 0) {
                        dialog.dataColumn.baseColumn1 = inputFirstColumn.selectedColumn
                        dialog.dataColumn.scale1      = inputFirstColumn.scale
                    }
                    if (dialog.requiredBaseColumns > 1) {
                        dialog.dataColumn.baseColumn2 = inputSecondColumn.selectedColumn
                        dialog.dataColumn.scale2      = inputSecondColumn.scale
                    }
                    dialog.dataColumn.defaultPrevious = (inputDefaultValueMode.selectedIndex === 0)

                    switch(dialog.dataColumn.type){
                    case DataColumn.GRADIENT:
                        switch (inputTimerangeMode.selectedIndex){
                        case 0:
                            dialog.dataColumn.referenceTime = 0 // compute absolute change from last row
                            break
                        case 1:
                            dialog.dataColumn.referenceTime = 1 // compute change per day from last row
                            break
                        }
                        break
                    case DataColumn.AGGREGATION:
                    case DataColumn.AVERAGE:
                        switch (inputTimerangeMode.selectedIndex){
                        case 0:
                            dialog.dataColumn.referenceTime = 0 // sum/average over all
                            break
                        case 1:
                            dialog.dataColumn.referenceTime = 7 // sum/average over last 7 days
                            break
                        case 2:
                            dialog.dataColumn.referenceTime = 30 // sum/average over last 30 days
                            break
                        }
                        break
                    }

                    // freeze and submit column to list model
                    dialog.freeze = true
                    root.dataColumnModel.insertLocalColumn(dialog.dataColumn)
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
