/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import MDTK 1.0 as MDTK

import "qrc:/qml/components"

import TSCore 1.0

Item {
    id: root
    anchors.fill: parent

    property int  tsUid: 0
    property bool openedWidthUid: false

    readonly property real hintHeight: bottomEdge.hint.height

    function show(tsUid){
        root.tsUid = tsUid
        root.openedWidthUid = true
        bottomEdge.commit()
    }

    BottomEdge {
        id: bottomEdge
        hint{
            status: root.enabled ? bottomEdge.showBottomEdgeHint ? BottomEdgeHint.Active : BottomEdgeHint.Inactive
                                 : BottomEdgeHint.Hidden
            iconName: "add"
            text: i18n.tr("New data row")
            onStatusChanged: {
                if (root.enabled && bottomEdge.showBottomEdgeHint)
                    hint.status = BottomEdgeHint.Active
            }
        }
        enabled: root.enabled
        readonly property bool showBottomEdgeHint: settings.showBottomEdgeHint
        onShowBottomEdgeHintChanged: hint.status = root.enabled ? showBottomEdgeHint ? BottomEdgeHint.Active : BottomEdgeHint.Inactive
                                                                : BottomEdgeHint.Hidden

        onCollapseCompleted: contentItem.reset()
        onCommitCompleted: {
            if (root.openedWidthUid)
                contentItem.selectTimeseries(root.tsUid)
            root.openedWidthUid = false
            contentItem.refresh()
        }

        contentComponent: MDTK.ThemedBottomEdgeContent {
            id: bottomEdgeContent
            height: root.height
            width:  root.width
            title: i18n.tr("New data row")

            property var dataRow: DataRow{}

            property real labelWidth: units.gu(6)

            function reset(){
                repeaterColumns.reset()
                dataRow.columns.clear()
            }

            function selectTimeseries(tsUid){
                tsSelector.selectedIndex = tsSelector.model.indexOf(tsUid)
            }

            function refresh(){
                // force the column items to refresh
                const selectedIdx = tsSelector.selectedIndex
                tsSelector.selectedIndex = -1
                tsSelector.selectedIndex = selectedIdx
            }

            head: TimeseriesSelector{
                id: tsSelector
                onSelectedTimeseriesChanged: bottomEdgeContent.dataRow.columns.clear()
            }


            Item{
                height: inputDate.height
                width: parent.contentWidth

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: i18n.tr("Date")
                    onWidthChanged: {
                        if (width > bottomEdgeContent.labelWidth)
                            bottomEdgeContent.labelWidth = width
                    }
                }

                MDTK.DateButton{
                    id: inputDate
                    anchors{
                        left: parent.left
                        right: parent.right
                        leftMargin: bottomEdgeContent.labelWidth + units.gu(2)
                    }
                    onDateChanged: bottomEdgeContent.dataRow.date = date
                }
            }

            Rectangle{
                width: parent.contentWidth
                height: units.gu(0.125)
                color: theme.palette.normal.foreground
            }

            Repeater{
                id: repeaterColumns
                model: tsSelector.selectedTimeseries ? tsSelector.selectedTimeseries.columns : 0

                function reset(){
                    for (var i=0; i<children.length; i++)
                        children[i].reset()
                }

                delegate: Item{
                    height: btValue.height
                    width: parent.contentWidth
                    visible: dataColumn.type === DataColumn.RAWDATA

                    Component.onCompleted: reset()
                    function reset(){
                        if (dataColumn.type === DataColumn.RAWDATA)
                            btValue.value = dataColumn.defaultPrevious ? tsSelector.selectedTimeseries.rows.getLastValue(dataColumn.uid)
                                                                       : dataColumn.defaultValue
                    }

                    Label{
                        anchors.verticalCenter: parent.verticalCenter
                        text: dataColumn.title
                        onWidthChanged: {
                            if (width > bottomEdgeContent.labelWidth)
                                bottomEdgeContent.labelWidth = width
                        }
                    }

                    DataColumnValueEditButton{
                        id: btValue
                        anchors{
                            left: parent.left
                            right: parent.right
                            leftMargin: bottomEdgeContent.labelWidth + units.gu(2)
                        }
                        onValueChanged: bottomEdgeContent.dataRow.columns.insertValue(dataColumn,value)
                    }
                }
            }

            Rectangle{
                width: parent.contentWidth
                height: units.gu(0.125)
                color: theme.palette.normal.foreground
            }

            MDTK.Button{
                id: btInsert
                anchors.horizontalCenter: parent.horizontalCenter
                width: 0.6*parent.contentWidth
                color: theme.palette.normal.positive
                _iconName: "ok"
                _text: i18n.tr("Insert row")
                _textColor: theme.palette.normal.positiveText

                onClicked: {
                    tsSelector.selectedTimeseries.insertRow(bottomEdgeContent.dataRow)
                    bottomEdge.collapse()
                }
            }
        }
    }
}
