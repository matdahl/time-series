/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import MDTK 1.0 as MDTK

import "qrc:/qml/components"

import TSCore 1.0

Item {
    id: root
    anchors.fill: parent

    // the time series to edit
    property var ts

    function show(ts){
        root.ts = ts
        TSBackend.timeseries.prepareTimeseriesForEdit(ts)
        bottomEdge.commit()
    }

    BottomEdge {
        id: bottomEdge
        hint.status: BottomEdgeHint.Hidden

        contentComponent: MDTK.ThemedBottomEdgeContent {
            id: bottomEdgeContent
            height: root.height
            width:  root.width

            // TRANSLATORS: time series is singular here
            title: i18n.tr("Edit time series")

            headerTrailingActionBar.actions: [
                Action{
                    iconName: "cancel"
                    onTriggered: bottomEdge.collapse()
                },
                Action{
                    iconName: "ok"
                    enabled: inputTitle.displayText.length > 0
                    onTriggered: {
                        root.ts.title = inputTitle.displayText
                        TSBackend.timeseries.update(root.ts)
                        bottomEdge.collapse()
                    }
                }
            ]

            MDTK.InputText{
                id: inputTitle
                title: i18n.tr("Name")
                width: parent.contentWidth
                placeholderText: i18n.tr("insert name ...")

                Binding{
                    target: inputTitle
                    property: "text"
                    value: root.ts.title
                }
            }

            Label{
                text: i18n.tr("Columns")
            }
            Column{
                width: parent.contentWidth
                Repeater{
                    id: repeaterColumns
                    model: TSBackend.timeseries.editColumns
                    delegate: ListItemDataColumn{
                        dataColumnModel: repeaterColumns.model
                        dragParent: bottomEdgeContent
                    }
                }
            }

            MDTK.Button{
                id: btNewColumn
                anchors.horizontalCenter: parent.horizontalCenter
                width: 0.6*parent.contentWidth
                _iconName: "add"
                _text: i18n.tr("Add column")

                onClicked: columnAddDialog.show()
            }

            DialogColumnAdd{
                id: columnAddDialog
                dataColumnModel: TSBackend.timeseries.editColumns
            }

            footer: Item{
                width: parent.contentWidth
                height: bottomEdgeNewDataRow.hintHeight
            }
        } // bottomEdgeContent
    } // bottomEdge
} // root
