/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import MDTK 1.0 as MDTK

import "qrc:/qml/components"

import TSCore 1.0

Item {
    id: root
    anchors.fill: parent

    function show(){
        bottomEdge.commit()
    }

    BottomEdge {
        id: bottomEdge
        hint.status: BottomEdgeHint.Hidden
        onCollapseCompleted: contentItem.reset()

        contentComponent: MDTK.ThemedBottomEdgeContent {
            id: bottomEdgeContent
            height: root.height
            width:  root.width

            // TRANSLATORS: time series is singular here
            title: i18n.tr("New time series")

            headerTrailingActionBar.actions: [
                Action{
                    iconName: "ok"
                    enabled: inputTitle.displayText.length > 0
                    onTriggered: {
                        TSBackend.timeseries.insert(inputTitle.displayText)
                        bottomEdge.collapse()
                    }
                }
            ]

            function reset(){
                inputTitle.text = ""
                TSBackend.timeseries.newColumns.clear()
            }

            MDTK.InputText{
                id: inputTitle
                title: i18n.tr("Name")
                width: parent.contentWidth
                placeholderText: i18n.tr("insert name ...")
            }

            Label{
                text: i18n.tr("Columns")
            }
            Column{
                width: parent.contentWidth
                Repeater{
                    id: repeaterColumns
                    model: TSBackend.timeseries.newColumns
                    delegate: ListItemDataColumn{
                        dataColumnModel: repeaterColumns.model
                        dragParent: bottomEdgeContent
                    }
                }
            }

            MDTK.Button{
                id: btNewColumn
                anchors.horizontalCenter: parent.horizontalCenter
                width: 0.6*parent.contentWidth
                _iconName: "add"
                _text: i18n.tr("Add column")

                onClicked: columnAddDialog.show()
            }

            DialogColumnAdd{
                id: columnAddDialog
                dataColumnModel: TSBackend.timeseries.newColumns
            }

            footer: Item{
                width: parent.contentWidth
                height: bottomEdgeNewDataRow.hintHeight
            }
        }
    }
}
