/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import "qrc:/utils/ColumnUtils.js" as Utils
import "qrc:/qml/components"

import TSCore 1.0

Item {
    id: root

    property var dataColumnModel
    property var dataColumn: DataColumn{}

    function show(){
        PopupUtils.open(dialogComponent)
    }

    Component{
        id: dialogComponent
        Dialog{
            id: dialog

            property int requiredBaseColumns: {
                switch (root.dataColumn.type){
                case DataColumn.AGGREGATION:
                case DataColumn.GRADIENT:
                case DataColumn.AVERAGE:
                    return 1
                case DataColumn.SUM:
                case DataColumn.DIFFERENCE:
                case DataColumn.PRODUCT:
                case DataColumn.QUOTIENT:
                    return 2
                }
                return 0
            }


            title: i18n.tr("Edit column")

            TextField{
                id: inputTitle
                placeholderText: i18n.tr("insert column name ...")
                text: root.dataColumn.title
                onTextChanged: root.dataColumn.title = text
            }

            // read-only
            OptionSelector{
                text: i18n.tr("Column type")
                model: [Utils.columnTitle(root.dataColumn.type)]
                enabled: false
                expanded: true
            }

            ColumnValueVisualisationEdit{
                dataColumn: root.dataColumn
            }

            OptionSelector{
                id: inputDefaultValueMode
                text: i18n.tr("Default value")
                visible: root.dataColumn.type === DataColumn.RAWDATA
                model: [
                    i18n.tr("Use previous value"),
                    i18n.tr("Manual value")
                ]
                expanded: true
                selectedIndex: root.dataColumn.defaultPrevious ? 0 : 1
                onSelectedIndexChanged: root.dataColumn.defaultPrevious = (selectedIndex === 0)
            }
            DataColumnValueEditButton{
                id: inputDefaultValue
                visible: root.dataColumn.type === DataColumn.RAWDATA && inputDefaultValueMode.selectedIndex === 1
                dataCol: root.dataColumn
                value: dataColumn.defaultValue
                onValueChanged: dataColumn.defaultValue = value
            }

            ReferenceColumnSelector{
                id: inputFirstColumn
                text: i18n.tr("First base column")
                visible: dialog.requiredBaseColumns > 0
                model: [root.dataColumnModel.titleAt(root.dataColumn.baseColumn1)]
                canEditColumn: false
                property bool initialised: false
                Component.onCompleted: {
                    setScale(dataColumn.scale1)
                    initialised = true
                }
                onScaleChanged: {
                    if (initialised)
                        dataColumn.scale1 = scale
                }
            }

            ReferenceColumnSelector{
                id: inputSecondColumn
                text: i18n.tr("Second base column")
                visible: dialog.requiredBaseColumns > 1
                model: [root.dataColumnModel.titleAt(root.dataColumn.baseColumn2)]
                canEditColumn: false
                property bool initialised: false
                Component.onCompleted: {
                    setScale(dataColumn.scale2)
                    initialised = true
                }
                onScaleChanged: {
                    if (initialised)
                        dataColumn.scale2 = scale
                }
            }

            Rectangle{
                width: dialog.contentWidth
                height: units.gu(0.25)
                color: theme.palette.normal.foreground
            }

            Button{
                text: i18n.tr("Done")
                color: LomiriColors.orange
                enabled: inputTitle.text.length > 0
                onClicked: PopupUtils.close(dialog)
            }
        }
    }
}
