/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timeseries is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unittests_datacolumnmodel.h"

void UnitTestsDataColumnModel::initTestCase(){
    m_model = new DataColumnModel();

    QCOMPARE(m_model->count(),0);

    QCOMPARE(m_model->idxAt( 1),-1);
    QCOMPARE(m_model->idxAt(-1),-1);
    QCOMPARE(m_model->titleAt( 1),QString());
    QCOMPARE(m_model->titleAt(-1),QString());
    QCOMPARE(m_model->getDependentIndices( 1),QVariantList());
    QCOMPARE(m_model->getDependentIndices(-1),QVariantList());
    QCOMPARE(m_model->getByIdx( 1),nullptr);
    QCOMPARE(m_model->getByIdx(-1),nullptr);
    QVERIFY(!m_model->contains(1));
}

void UnitTestsDataColumnModel::cleanupTestCase(){
    m_model->clear();
    QCOMPARE(m_model->count(),0);

    DataColumn *col1 = new DataColumn(this);
    col1->setUid(42);
    col1->setIdx(1);
    m_model->insertColumn(col1);

    DataColumn *col2 = new DataColumn(m_model);
    col2->setUid(40);
    col2->setIdx(2);
    m_model->insertColumn(col2);

    QCOMPARE(m_model->count(),2);
    QCOMPARE(m_model->list().at(0)->uid(),42);
    QCOMPARE(m_model->list().at(1)->uid(),40);
    QCOMPARE(m_model->list().at(0)->parent(),this);
    QCOMPARE(m_model->list().at(1)->parent(),m_model);

    delete m_model;

    // col1 must still exists
    QCOMPARE(col1->uid(),42);
    QCOMPARE(col2->uid(),40);
}
