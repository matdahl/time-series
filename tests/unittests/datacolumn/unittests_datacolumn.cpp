/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timeseries is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unittests_datacolumn.h"

void UnitTestsDataColumn::initTestCase(){
    m_col = new DataColumn;

    QCOMPARE(m_col->uid(),0);
    QCOMPARE(m_col->type(),DataColumn::RAWDATA);
    QCOMPARE(m_col->referenceTime(),0);
}

void UnitTestsDataColumn::gradientMode(){

    // since col is by default of type RAWDATA, its gradient mode must be invalid
    QCOMPARE(m_col->gradientMode(),DataColumn::GRADIENT_INVALID);

    // try all column types except GRADIENT. For all, the mode should be invalid
    for (int i=0; i<DataColumn::NUMBER_OF_TYPES; i++){
        if (i == DataColumn::GRADIENT)
            continue;

        m_col->setType((DataColumn::Type) i);
        QCOMPARE(m_col->gradientMode(),DataColumn::GRADIENT_INVALID);
    }

    // when setting type to GRADIENT, the mode should become GRADIENT_ABSOLUTE
    m_col->setType(DataColumn::GRADIENT);
    QCOMPARE(m_col->gradientMode(),DataColumn::GRADIENT_ABSOLUTE);

    // when referenceTime is 1, we should get GRADIENT_PERDAY, for anything else than 0 or 1 we should be INVALID again
    for (int i = -100; i<=100; i++){
        m_col->setReferenceTime(i);
        switch (i) {
        case 0:
            QCOMPARE(m_col->gradientMode(),DataColumn::GRADIENT_ABSOLUTE);
            break;
        case 1:
            QCOMPARE(m_col->gradientMode(),DataColumn::GRADIENT_PERDAY);
            break;
        default:
            QCOMPARE(m_col->gradientMode(),DataColumn::GRADIENT_INVALID);
            break;
        }
    }

    // make sure, m_col is again a default constructed DataColumn
    delete m_col;
    m_col = new DataColumn;
}

void UnitTestsDataColumn::cleanupTestCase(){
    delete m_col;
}
